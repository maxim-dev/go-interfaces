package main

type Bike struct {
	Power Consumable
}

func (b *Bike) RideBike() (vroom bool) {
	return b.Power.Consume()
}

func NewBike(power int) Rideable {
	return &Bike{
		Power: &Consumer{power},
	}
}
