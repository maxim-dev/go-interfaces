package main

type Person struct {
	IsWake bool
	Rideable
	Shootable
}

func (p *Person) RideBike() (vroom bool) {
	if !p.IsWake {
		return
	}

	return p.Rideable.RideBike()
}

func (p *Person) Shoot() (bang bool) {
	if !p.IsWake {
		return
	}

	return p.Shootable.Shoot()
}
