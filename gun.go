package main

type Gun struct {
	Ammo int
}

func (g *Gun) Shoot() (bang bool) {
	return Consume(&g.Ammo)
}

func (g *Gun) GetColor() string {
	return "Gun color"
}

type Rocket struct {
	Missels int
}

func (r *Rocket) Shoot() (bang bool) {
	return
}

//func (r *Rocket) GetColor() string {
//	return "Rocket color"
//}
