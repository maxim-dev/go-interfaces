package main

import (
	"fmt"
)

func main() {

	p := &Person{
		true,
		NewBike(1),
		&Gun{2},
	}

	p.Shootable = &Rocket{}

	fmt.Printf("%v", p.Shootable.(*Rocket).Missels)

	switch shootable := p.Shootable.(type) {
	case *Gun:
		fmt.Println(shootable.Ammo)
	case *Rocket:
		fmt.Println(shootable.Missels)

	}

	//fmt.Println(p.Shoot(), p.RideBike(), p)
	//fmt.Println(p.Shoot(), p.RideBike(), p)
	//fmt.Println(p.Shoot(), p.RideBike(), p)
	//fmt.Println(p.Shoot(), p.RideBike(), p)
}
