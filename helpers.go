package main

func Consume(supply *int) (done bool) {
	if *supply <= 0 {
		return
	}

	*supply--

	return true
}
