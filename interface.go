package main

type Rideable interface {
	RideBike() bool
}

type Shootable interface {
	Shoot() (bang bool)
}

type Consumable interface {
	Consume() bool
}

type Colorable interface {
	GetColor() string
}
