package main

type Consumer struct {
	supply int
}

func (c *Consumer) Consume() (done bool) {
	if c.supply <= 0 {
		return
	}

	c.supply--

	return true
}
