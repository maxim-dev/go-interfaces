package main

type Tank struct {
}

func (t *Tank) Shoot() bool {
	return false
}

func (t *Tank) GetColor() string {
	return "Tank color"
}
